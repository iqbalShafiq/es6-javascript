/**
 * * Arrow Function
 *  */

const golden = () => console.log("this is golden!!");
golden();

/**
 * * Object Literal
 *  */

const newFunction = {
	firstName: "William",
	lastName: "Imoh",
	fullName: function () {
		console.log(this.firstName + " " + this.lastName);
	},
};
//Driver Code
newFunction.fullName();

/**
 * * Destructuring
 *  */
let newObject = {
	firstName: "Harry",
	lastName: "Potter Holt",
	destination: "Hogwarts React Conf",
	occupation: "Deve-wizard Avocado",
	spell: "Vimulus Renderus!!!",
};
const { firstName, lastName, destination, occupation, spell } = newObject;
// Driver code
console.log(firstName, lastName, destination, occupation);

/**
 * * Array Spreading
 *  */
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];
//Driver Code
console.log(combined);

/**
 * * Template Literals
 *  */
const planet = "earth";
const view = "glass";
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`;
// Driver Code
console.log(before);
